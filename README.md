# Lib

Library of generally useful code.

- [**crypt**](./crypt) - simple cryptography
- [**uuid**](./uuid) - simple uuid generation
