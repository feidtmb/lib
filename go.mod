module gitlab.com/feidtmb/lib

go 1.23

require (
	github.com/elithrar/simple-scrypt v1.3.0
	golang.org/x/crypto v0.26.0
)
