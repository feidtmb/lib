package crypt_test

import (
	"reflect"
	"testing"

	"gitlab.com/feidtmb/lib/crypt"
)

func TestPrivateKey_Sign(t *testing.T) {
	priv := crypt.NewPrivateKey()
	pub := priv.PublicKey()
	msg := []byte("test message")

	sig, err := priv.Sign(msg)
	if err != nil {
		t.Fatalf("Failed to sign message: %v", err)
	}

	if err := pub.Verify(msg, sig); err != nil {
		t.Fatalf("Failed to verify signed message: %v", err)
	}

	if err := pub.Verify([]byte("fake message"), sig); err == nil {
		t.Error("Verified fake message")
	}

	if err := pub.Verify(msg, []byte("fake sig")); err == nil {
		t.Error("Verified message with fake signature")
	}
}

func TestPrivateKey_MarshalText(t *testing.T) {
	priv := crypt.NewPrivateKey()

	text, err := priv.MarshalText()
	if err != nil {
		t.Fatal(err)
	}

	var parsed crypt.PrivateKey
	if err := parsed.UnmarshalText(text); err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(priv, parsed) {
		t.Fatal("Parsed key doesn't match original")
	}
}

func TestPublicKey_MarshalText(t *testing.T) {
	pub := crypt.NewPrivateKey().PublicKey()

	text, err := pub.MarshalText()
	if err != nil {
		t.Fatal(err)
	}

	var parsed crypt.PublicKey
	if err := parsed.UnmarshalText(text); err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(pub, parsed) {
		t.Fatal("Parsed public key doesn't match original")
	}
}
