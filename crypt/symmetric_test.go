package crypt_test

import (
	"reflect"
	"testing"

	"gitlab.com/feidtmb/lib/crypt"
)

func TestNewSymmetricKeyFromSecret(t *testing.T) {
	secrets := [][]byte{
		nil,
		[]byte(""),
		[]byte("test"),
		[]byte("a pretty long string with some numbers 123 and symbols $*@^'.'|!`|_% and stuff"),
	}

	keyMap := make(map[crypt.SymmetricKey]int)

	for _, secret := range secrets {
		t.Run(string(secret), func(t *testing.T) {
			for _, salt := range secrets {
				t.Run(string(salt), func(t *testing.T) {
					expectErr := len(secret) == 0 || len(salt) == 0

					key, err := crypt.NewSymmetricKeyFromSecret(secret, salt)
					if err != nil {
						if !expectErr {
							t.Fatalf("Error generating key: %v", err)
						}
						return
					}

					if expectErr {
						t.Fatalf("Expected error from nil or empty secret or salt")
					}

					keyMap[key]++

					if reflect.DeepEqual(key, secret) {
						t.Errorf("Expected key not to match secret")
					}
					if len(key) != 32 {
						t.Errorf("Expected key to be length 32, got length %d", len(key))
					}
				})
			}
		})
	}

	for key, count := range keyMap {
		if count > 1 {
			t.Errorf("Generated key %q %d times from different inputs.", key, count)
		}
	}
}

func TestSymmetricKey_Encrypt(t *testing.T) {
	key := crypt.NewSymmetricKey()
	msg := []byte("test message")

	encrypted, err := key.Encrypt(msg)
	if err != nil {
		t.Fatalf("Failed to encrypt message: %v", err)
	}

	if reflect.DeepEqual(encrypted, msg) {
		t.Fatalf("Encrypted message matches input.")
	}
	if len(encrypted) == len(msg) {
		t.Fatalf("Encrypted message is same length as plaintext.")
	}

	decrypted, err := key.Decrypt(encrypted)
	if err != nil {
		t.Fatalf("Failed to decrypt message: %v", err)
	}

	if !reflect.DeepEqual(decrypted, msg) {
		t.Fatalf("Decrypted message doesn't match input.")
	}
}

func TestSymmetricKey_Sign(t *testing.T) {
	key := crypt.NewSymmetricKey()
	msg := []byte("test message")

	sig := key.Sign(msg)

	if !key.Verify(msg, sig) {
		t.Fatal("Failed to verify signed message")
	}

	if key.Verify([]byte("fake message"), sig) {
		t.Error("Verified fake message")
	}

	if key.Verify(msg, []byte("fake sig")) {
		t.Error("Verified message with fake signature")
	}
}

func TestSymmetricKey_MarshalText(t *testing.T) {
	k := crypt.NewSymmetricKey()

	text, err := k.MarshalText()
	if err != nil {
		t.Fatal(err)
	}

	var parsed crypt.SymmetricKey
	if err := parsed.UnmarshalText(text); err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(k, parsed) {
		t.Fatal("Parsed key doesn't match original")
	}
}
