package crypt

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"time"
)

type token struct {
	KeyID     string `json:"k,omitempty"`
	Issued    int64  `json:"i"`
	Expires   int64  `json:"e"`
	Payload   []byte `json:"p"`
	Signature []byte `json:"s"`
}

// Token returns a URL-safe signed token string containing the provided key ID
// (if any), payload, issued time (with second precision), and expiration time.
//
// ParseToken can be used to extract the verified payload and issued time.
// KeyIDFromToken can be used to extract only the (unverified) key ID from the
// token, which is useful if there are multiple keys with which the token may
// have been signed.
//
// Note that the token is not encrypted, so the payload should not contain
// sensitive data, only data that needs to be verified. If sensitive data needs
// to be stored in the token, it should be encrypted separately and the result
// provided as the token payload.
func Token(key SymmetricKey, keyID string, payload []byte, issued time.Time, expires time.Duration) (string, error) {
	t := token{
		KeyID:   keyID,
		Issued:  issued.Unix(),
		Expires: issued.Add(expires).Unix(),
		Payload: payload,
	}

	msg, err := json.Marshal(t)
	if err != nil {
		return "", err
	}

	t.Signature = key.Sign(msg)

	bs, err := json.Marshal(t)
	if err != nil {
		return "", err
	}

	return base64.URLEncoding.EncodeToString(bs), nil
}

// ErrTokenExpired is the error returned when attemping to parse an expired
// token.
var ErrTokenExpired = errors.New("token expired")

// ParseToken verifies that the provided token string was signed by the provided
// key and is not expired, then returns the original payload and the time the
// token was issued.
func ParseToken(key SymmetricKey, tok string) (payload []byte, issued time.Time, err error) {
	t, err := parseToken(tok)
	if err != nil {
		return nil, time.Time{}, err
	}

	sig := t.Signature
	t.Signature = nil

	msg, err := json.Marshal(t)
	if err != nil {
		return nil, time.Time{}, err
	}

	if !key.Verify(msg, sig) {
		return nil, time.Time{}, errors.New("invalid signature")
	}

	if time.Now().After(time.Unix(t.Expires, 0)) {
		return nil, time.Time{}, ErrTokenExpired
	}

	return t.Payload, time.Unix(t.Issued, 0), nil
}

// ErrTokenMissingKeyID is the error returned when attempting to extract a key
// ID from a token that does not contain one.
var ErrTokenMissingKeyID = errors.New("token does not contain a key ID")

// KeyIDFromToken extracts a key ID from a token, or returns the
// ErrTokenMissingKeyID error if the token does not contain a key ID.
func KeyIDFromToken(tok string) (string, error) {
	t, err := parseToken(tok)
	if err != nil {
		return "", err
	}

	if t.KeyID == "" {
		return "", ErrTokenMissingKeyID
	}

	return t.KeyID, nil
}

func parseToken(tok string) (token, error) {
	if tok == "" {
		return token{}, errors.New("empty token")
	}

	bs, err := base64.URLEncoding.DecodeString(tok)
	if err != nil {
		return token{}, err
	}

	var t token
	if err := json.Unmarshal(bs, &t); err != nil {
		return token{}, err
	}

	return t, nil
}
