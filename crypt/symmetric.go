package crypt

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"errors"

	"golang.org/x/crypto/scrypt"
)

// SymmetricKey represents a symmetric key.
//
// A symmetric key can be used to encrypt and decrypt messages, as well as sign
// and verify messages.
type SymmetricKey [32]byte

// NewSymmetricKey returns a random symmetric key.
func NewSymmetricKey() SymmetricKey {
	var k SymmetricKey
	if _, err := rand.Read(k[:]); err != nil {
		// Shouldn't happen.
		panic(err)
	}
	return k
}

// NewSymmetricKeyFromSecret uses the provided secret and salt for a new
// symmetric key.
func NewSymmetricKeyFromSecret(secret []byte, salt []byte) (SymmetricKey, error) {
	var k SymmetricKey

	if len(secret) == 0 || len(salt) == 0 {
		return k, errors.New("secret and salt are required and may not be empty")
	}

	sk, err := scrypt.Key(secret, salt, 32768, 8, 1, 32)
	copy(k[:], sk)

	return k, err
}

// Encrypt encrypts a message.
func (k SymmetricKey) Encrypt(msg []byte) ([]byte, error) {
	block, err := aes.NewCipher(k[:])
	if err != nil {
		return nil, err
	}

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	nonce := make([]byte, aesgcm.NonceSize())
	if _, err = rand.Read(nonce); err != nil {
		return nil, err
	}

	ciphertext := aesgcm.Seal(nil, nonce, msg, nil)

	return append(nonce, ciphertext...), nil
}

// Decrypt decypts a message.
func (k SymmetricKey) Decrypt(msg []byte) ([]byte, error) {
	block, err := aes.NewCipher(k[:])
	if err != nil {
		return nil, err
	}

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	nonce, ciphertext := msg[:aesgcm.NonceSize()], msg[aesgcm.NonceSize():]

	plaintext, err := aesgcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		return nil, err
	}

	return plaintext, nil
}

// Sign signs a message using the symmetric key.
func (k SymmetricKey) Sign(msg []byte) []byte {
	mac := hmac.New(sha256.New, k[:])
	mac.Write(msg)
	return mac.Sum(nil)
}

// Verify verifies that a message was signed using the symmetric key.
func (k SymmetricKey) Verify(msg []byte, sig []byte) bool {
	mac := hmac.New(sha256.New, k[:])
	mac.Write(msg)

	return hmac.Equal(sig, mac.Sum(nil))
}

// String returns the symmetric key as a base64 encoded string.
func (k SymmetricKey) String() string {
	text, err := k.MarshalText()
	if err != nil {
		return ""
	}
	return string(text)
}

// MarshalText returns the symmetric key base64 encoded.
func (k SymmetricKey) MarshalText() ([]byte, error) {
	text := base64.StdEncoding.EncodeToString(k[:])
	return []byte(text), nil
}

// UnmarshalText unmarshals a base64 encoded symmetric key.
func (k *SymmetricKey) UnmarshalText(text []byte) error {
	sk, err := base64.StdEncoding.DecodeString(string(text))
	if err != nil {
		return err
	}

	if len(sk) != cap(k) {
		return errors.New("invalid key length")
	}

	copy(k[:], sk)
	return nil
}
