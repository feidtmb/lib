package crypt

import (
	scrypt "github.com/elithrar/simple-scrypt"
)

// OneWayHash generates a hash of a secret, which can be verified when supplied
// with the same secret, but not easily reversed.
func OneWayHash(secret []byte) ([]byte, error) {
	return scrypt.GenerateFromPassword(secret, scrypt.DefaultParams)
}

// MatchesOneWayHash returns true if the secret matches the hash. For use with
// OneWayHash.
func MatchesOneWayHash(secret []byte, hash []byte) bool {
	return scrypt.CompareHashAndPassword(hash, secret) == nil
}
