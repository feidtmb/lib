package crypt_test

import (
	"encoding/base64"
	"encoding/json"
	"math"
	"reflect"
	"testing"
	"time"

	"gitlab.com/feidtmb/lib/crypt"
)

func TestToken(t *testing.T) {
	key := crypt.NewSymmetricKey()
	kid := "fake-key-id"
	now := time.Now().Truncate(time.Second)

	unrelatedKey := crypt.NewSymmetricKey()

	cases := []struct {
		name    string
		payload interface{}
		expires time.Duration
	}{
		{
			name:    "string payload",
			payload: "This is a test message.",
			expires: time.Minute,
		},
		{
			name:    "long string payload",
			payload: "This is a longer test message. It has multiple sentences. This is a longer test message. It has multiple sentences. This is a longer test message. It has multiple sentences. This is a longer test message. It has multiple sentences. This is a longer test message. It has multiple sentences. This is a longer test message. It has multiple sentences.",
			expires: time.Minute,
		},
		{
			name:    "int payload",
			payload: 13,
			expires: time.Minute,
		},
		{
			name:    "max int64 payload",
			payload: math.MaxInt64,
			expires: time.Minute,
		},
		{
			name:    "float payload",
			payload: 12.34567,
			expires: time.Minute,
		},
		{
			name: "struct payload",
			payload: struct {
				A string
				B int
			}{
				A: "Inner message.",
				B: 2020,
			},
			expires: time.Minute,
		},
		{
			name:    "expired",
			payload: "Should be expired...",
			expires: -time.Minute,
		},
	}

	for _, c := range cases {
		c := c // Capture range variable.
		t.Run(c.name, func(t *testing.T) {
			t.Parallel()

			p, err := json.Marshal(c.payload)
			if err != nil {
				t.Fatal(err)
			}

			token, err := crypt.Token(key, "", p, now, c.expires)
			if err != nil {
				t.Fatal(err)
			}

			tokenWithKeyID, err := crypt.Token(key, kid, p, now, c.expires)
			if err != nil {
				t.Fatal(err)
			}

			_, err = crypt.KeyIDFromToken(token)
			if err != crypt.ErrTokenMissingKeyID {
				t.Fatal("Expected token missing key ID error")
			}

			keyID, err := crypt.KeyIDFromToken(tokenWithKeyID)
			if err != nil {
				t.Fatal(err)
			}
			if keyID != kid {
				t.Fatalf("Expected key ID %s, got %s", kid, keyID)
			}

			payload, issued, err := crypt.ParseToken(key, token)
			switch err {
			case nil:
				if c.expires < 0 {
					t.Fatal("Expected token to be expired")
				}
			case crypt.ErrTokenExpired:
				if c.expires > 0 {
					t.Fatal("Expected token not to be expired")
				}
				return
			default:
				t.Fatal(err)
			}

			if !issued.Equal(now) {
				t.Fatalf("Expected issued time %v, got %v", now, issued)
			}

			if !reflect.DeepEqual(payload, p) {
				t.Fatalf("Expected token value %s to match pre-token value %s", payload, p)
			}

			// Test using the wrong public key
			if _, _, err := crypt.ParseToken(unrelatedKey, token); err == nil {
				t.Fatal("Expected error from using wrong public key")
			}

			// Test tampered token
			decoded, err := base64.URLEncoding.DecodeString(token)
			if err != nil {
				t.Fatal(err)
			}
			var tampering struct {
				KeyID     string `json:"k,omitempty"`
				Issued    int64  `json:"i"`
				Expires   int64  `json:"e"`
				Payload   []byte `json:"p"`
				Signature []byte `json:"s"`
			}
			if err := json.Unmarshal(decoded, &tampering); err != nil {
				t.Fatal(err)
			}
			tampering.Expires = time.Now().Add(time.Hour * 72).Unix()
			tamperedJSONBytes, err := json.Marshal(tampering)
			if err != nil {
				t.Fatal(err)
			}
			tampered := base64.URLEncoding.EncodeToString(tamperedJSONBytes)
			if _, _, err := crypt.ParseToken(key, tampered); err == nil {
				t.Fatal("Expected error from tampered token")
			}
		})
	}
}
