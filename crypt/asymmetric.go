package crypt

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha512"
	"crypto/x509"
	"encoding/base64"
	"errors"
)

// PrivateKey represents a private RSA key.
//
// A private key can be used to sign a message, and its associated public key
// can be used to verify messages as having been signed by the private key.
type PrivateKey struct {
	rsa *rsa.PrivateKey
}

// PublicKey represents a public RSA key.
//
// A public key can be used to verify messages as having been signed by the
// associated private key.
type PublicKey struct {
	rsa *rsa.PublicKey
}

// NewPrivateKey returns a random private key.
func NewPrivateKey() PrivateKey {
	k, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		// Shouldn't happen.
		panic(err)
	}

	return PrivateKey{rsa: k}
}

// Sign signs a message using the private key. The public key part of the key
// pair may be used to verify the signature.
func (priv PrivateKey) Sign(msg []byte) ([]byte, error) {
	hashed := sha512.Sum512(msg)
	return rsa.SignPSS(rand.Reader, priv.rsa, crypto.SHA512, hashed[:], nil)
}

// PublicKey returns the public key part of the RSA key pair.
func (priv PrivateKey) PublicKey() PublicKey {
	k := priv.rsa.PublicKey
	return PublicKey{rsa: &k}
}

// String returns a base64 encoded PKCS #8, ASN.1 DER form RSA private key.
func (priv PrivateKey) String() string {
	text, err := priv.MarshalText()
	if err != nil {
		return ""
	}
	return string(text)
}

// MarshalText returns a base64 encoded PKCS #8, ASN.1 DER form RSA private key.
func (priv PrivateKey) MarshalText() ([]byte, error) {
	der, err := x509.MarshalPKCS8PrivateKey(priv.rsa)
	if err != nil {
		return nil, err
	}

	text := base64.StdEncoding.EncodeToString(der)
	return []byte(text), nil
}

// UnmarshalText unmarshals a base64 encoded PKCS #8, ASN.1 DER form RSA private
// key.
func (priv *PrivateKey) UnmarshalText(text []byte) error {
	der, err := base64.StdEncoding.DecodeString(string(text))
	if err != nil {
		return err
	}

	k, err := x509.ParsePKCS8PrivateKey(der)
	if err != nil {
		return err
	}

	rsaK, ok := k.(*rsa.PrivateKey)
	if !ok {
		return errors.New("unexpected key type")
	}

	priv.rsa = rsaK
	return nil
}

// Verify verifies that a message was signed using the private key part of the
// key pair.
func (pub PublicKey) Verify(msg []byte, sig []byte) error {
	hashed := sha512.Sum512(msg)
	return rsa.VerifyPSS(pub.rsa, crypto.SHA512, hashed[:], sig, nil)
}

// String returns a base64 encoded PKIX, ASN.1 DER form RSA public key.
func (pub PublicKey) String() string {
	text, err := pub.MarshalText()
	if err != nil {
		return ""
	}
	return string(text)
}

// MarshalText returns a base64 encoded PKIX, ASN.1 DER form RSA public key.
func (pub PublicKey) MarshalText() ([]byte, error) {
	der, err := x509.MarshalPKIXPublicKey(pub.rsa)
	if err != nil {
		return nil, err
	}

	text := base64.StdEncoding.EncodeToString(der)
	return []byte(text), nil
}

// UnmarshalText unmarshals a base64 encoded PKIX, ASN.1 DER form RSA public
// key.
func (pub *PublicKey) UnmarshalText(text []byte) error {
	der, err := base64.StdEncoding.DecodeString(string(text))
	if err != nil {
		return err
	}

	k, err := x509.ParsePKIXPublicKey(der)
	if err != nil {
		return err
	}

	rsaK, ok := k.(*rsa.PublicKey)
	if !ok {
		return errors.New("unexpected key type")
	}

	pub.rsa = rsaK
	return nil
}
