package crypt_test

import (
	"testing"

	"gitlab.com/feidtmb/lib/crypt"
)

func TestOneWayHash(t *testing.T) {
	in := []byte("test input")

	hash, err := crypt.OneWayHash(in)
	if err != nil {
		t.Fatalf("Failed to hash input: %v", err)
	}

	if !crypt.MatchesOneWayHash(in, hash) {
		t.Fatal("Failed to match input against hash")
	}

	if crypt.MatchesOneWayHash([]byte("fake input"), hash) {
		t.Error("Matched fake input against hash")
	}

	if crypt.MatchesOneWayHash(in, []byte("fake hash")) {
		t.Error("Matched input against fake hash")
	}
}
