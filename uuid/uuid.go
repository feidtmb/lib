// Package UUID defines a simple function for generating a UUID.
package uuid

import (
	"crypto/rand"
	"fmt"
	"io"
)

// New generates a new UUID.
//
// The generated UUID is version 4 (random data based), variant 10 (DCE 1.1, ISO/IEC 11578:1996).
//
// Panics if unable to read from crypto/rand.Reader.
func New() string {
	// Read 16 random bytes.
	var bs [16]byte
	_, err := io.ReadFull(rand.Reader, bs[:])
	if err != nil {
		panic(err)
	}

	// Set 13th hex digit to 4 (signifying uuid version 4).
	bs[6] = (bs[6] & 0x0f) | 0x40 // the 7th byte contains the 13th and 14th hex digits; we discard the 13th and replace with hex 4
	// Set 17th hex digit to begin with binary bits 10 (signifying uuid variant 10).
	bs[8] = (bs[8] & 0b00111111) | 0b10000000 // the 9th byte contains the 17th and 18th hex digits; we discard the first 2 bits of the 17th and replace with binary 10

	// Return bytes in uuid format.
	return fmt.Sprintf("%x-%x-%x-%x-%x", bs[:4], bs[4:6], bs[6:8], bs[8:10], bs[10:])
}
